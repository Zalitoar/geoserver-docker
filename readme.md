Copy prod directory to:
/var/lib/docker/volumes/

Set the permissions of the copied directory recursively:
sudo chown -R root:staff /var/lib/docker/volumes/prod

Give execute access to the setenv.sh file:
sudo chmod 777 /var/lib/docker/volumes/prod/tomcat/setenv.sh

Execute the following command to run the container (add -d at his end to avoid the interactive method)
docker-compose up

The container should be running according to the parameters in the docker-compose.yml file.

Review the execution with (-f for make it interactive):
docker-compose logs -f
or
docker logs -f (name of container)

